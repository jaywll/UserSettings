;
; Usage:	UserSettings.exe [flags]
;
; Flags:
; 	/?				Display help message. Note that all other flags are ignored if this is specified.
;
;	/v				Verbose - Display error and debug messages (Default: Fail silently, no debug messages)
;	/e				Errors - Display error messages (Default: Fail silently)
;	/s				Silent - Don't display progress window (Default: Display progress window if action is required)
;	Note that the above three flags are contracdictory. If more than one is specified, whichever comes last overrides its predecessors
;
;	/f				Force - Ignore history of previously applied items and re-apply all configurations
;
;	-o ConfigOU		Specify the active directory organizational unit that contains settings groups (Default: "Computer Configuration Groups")
;	-l Directory	Specify the folder that contains the .ini files for each group (Default: Same directory as executable)
;	-c Directory	Spefify the folder that contains the cache .ini file (Default: %APPDATA%\UserSettingsTool)
;

#include <MsgBoxConstants.au3>
#include <GUIConstantsEx.au3>
#include <EditConstants.au3>
#include <GuiListView.au3>
#include <ad/AD.au3>

Global $idListView, $idVerboseExit

; Define default settings
$displayHelp = False
$reportingLevel = 1
$forceInstall = False
$configOU = "Computer Configuration Groups"
$iniLocation = @ScriptDir
$cacheLocation = EnvGet("APPDATA") & "\UserSettingsTool"
$cacheFile = $cacheLocation & "\AppliedSettings.ini"
$currentUser = StringLower(@UserName)
$msgBoxTitle = "User Settings Application Tool"

; Loop through the command line flags and overwrite defaults with user-specified settings
If $CmdLine[0] > 0 Then
   $i = 1

   Do
	  If $CmdLine[$i] = "/?" Then
		 $displayHelp = True
		 $i += 1
	  ElseIf $CmdLine[$i] = "/v" Then
		 $reportingLevel = 3
		 $i += 1
	  ElseIf $CmdLine[$i] = "/e" Then
		 $reportingLevel = 2
		 $i += 1
	  ElseIf $CmdLine[$i] = "/s" Then
		 $reportingLevel = 0
		 $i += 1
	  ElseIf $CmdLine[$i] = "/f" Then
		 $forceInstall = True
		 $i += 1
	  ElseIf $CmdLine[$i] = "-o" Then
		 $configOU = $CmdLine[$i + 1]
		 $i += 2
	  ElseIf $CmdLine[$i] = "-l" Then
		 $iniLocation = $CmdLine[$i + 1]
		 $i += 2
	  ElseIf $CmdLine[$i] = "-c" Then
		 $cacheLocation = $CmdLine[$i + 1]
		 $i += 2
	  Else
		 $i += 1
	  EndIf
   Until $i = UBound($CmdLine)
EndIf

; Display help message if /? flag is set
If $displayHelp Then
   DisplayHelp()
EndIf

; Create and display verbose output window if /v flag is set
If $reportingLevel = 3 Then
   DisplayVerbose()

   ; DISPLAY INITIAL LOG OF SETTINGS HERE
EndIf

; Connect to Active Directory
_AD_Open()
If @Error Then
   CriticalStop("Could not connect to the domain controller to fetch configuration groups for this host.")
   AllComplete(11)
EndIf

; Get all the computer's groups
$aAllGroups = _AD_GetUserGroups(@ComputerName & "$")
If @Error Then
   If $reportingLevel > 2 Then AddOutputLine("Nothing to do. This computer is not part of any active directory groups.")
   AllComplete(1)
EndIf

; Close Active Directory Connection
_AD_Close()

; Loop through each group and add those that are relevant (i.e they're in $configOU) to a new array for further action
Local $aActionGroups[0]

For $i = 1 to UBound($aAllGroups) - 1
   If StringInStr($aAllGroups[$i] & ",", "OU=" & $configOU & ",") > 0 Then
	  ReDim $aActionGroups[UBound($aActionGroups) + 1]
	  $aActionGroups[UBound($aActionGroups) - 1] = StringMid($aAllGroups[$i], 4, (StringInStr($aAllGroups[$i], ",") - 4))
   EndIf
Next

If UBound($aActionGroups) = 0 Then
   If $reportingLevel > 2 Then AddOutputLine("Nothing to do. This computer is not part of any applicable configuration groups.")
   AllComplete(1)
EndIf

; Loop through each of the groups from $configOU and attempt open the .ini file. Put any actions in the $aActions array (title, command)
Local $aActions[0][2]
For $i = 0 to UBound($aActionGroups) - 1
   Local $aSectionActions[0][2]
   $aData = IniReadSection($iniLocation & "\" & $aActionGroups[$i] & ".ini", $currentUser)

   If @Error Then
	  If $reportingLevel > 2 Then AddOutputLine("There is no action necessary for user " & $currentUser & " in configuration group " & $aActionGroups[$i] & "." & @CRLF & @CRLF & "Either no .ini file exists for the group " & $aActionGroups[$i] & ", or there is no [" & $currentUser & "] section within it.")
   Else
	  For $n = 1 To UBound($aData) - 1
		 ReDim $aSectionActions[UBound($aSectionActions) + 1][2]
		 $aSectionActions[UBound($aSectionActions) - 1][0] = $aData[$n][0]
		 $aSectionActions[UBound($aSectionActions) - 1][1] = $aData[$n][1]
	  Next
   EndIf

   If UBound($aSectionActions) > 0 Then
	  ReDim $aActions[UBound($aActions) + 1][2]
	  $aActions[UBound($aActions) - 1][0] = $aActionGroups[$i]
	  $aActions[UBound($aActions) - 1][1] = $aSectionActions
   EndIf
Next

If UBound($aActions) = 0 Then
   If $reportingLevel > 2 Then AddOutputLine("Nothing to do. None of the configuration groups this computer is a part of have any actions defined for the user " & $currentUser & ".")
   AllComplete(1)
EndIF

; Does $cacheLocation exist?
If FileExists($cacheLocation) = 0 Then
   If $reportingLevel > 2 Then AddOutputLine("The specified cache folder, " & $cacheLocation & ", does not exist. It will be created.")
   DirCreate($cacheLocation)
EndIf

; Does $cacheFile exist?
If FileExists($cacheFile) = 0 Then
   If $reportingLevel > 2 Then AddOutputLine("The cache of previously applied settings does not exist. All actions will be run, and it will be created.")
   $forceInstall = True
ElseIf $forceInstall Then
   If $reportingLevel > 2 Then AddOutputLine("The cache of previously applied settings will be deleted as a result of the /f command line option used. All actions will be run, and it will be recreated.")
   FileDelete($cacheFile)
EndIf

; If we're not forcing install, conduct match and kill on $aActions. We can't delete keys while looping though, so we'll make a list in $deleteKeys
If $forceInstall = False Then
   Local $deleteKeys[0][2]
   For $i = 0 to UBound($aActions) - 1
	  For $n = 0 to UBound($aActions[$i][1]) - 1
		 If IniRead ($cacheFile, $aActions[$i][0], ($aActions[$i][1])[$n][0], "") = 1 Then
			ReDim $deleteKeys[UBound($deleteKeys) + 1][2]
			$deleteKeys[UBound($deleteKeys) - 1][0] = $i
			$deleteKeys[UBound($deleteKeys) - 1][1] = $n
		 EndIF
	  Next
   Next

   For $i = UBound($deleteKeys) - 1 to 0 Step -1
	  _ArrayDelete($aActions[$deleteKeys[$i][0]][1], $deleteKeys[$i][1])
   Next

   ; Loop through $aActions again, and delete any top-level keys that are empty as a result of all their bottom-level keys being removed
   ReDim $deleteKeys[0]
   For $i = 0 to UBound($aActions) - 1
	  If UBound($aActions[$i][1]) = 0 Then
		 ReDim $deleteKeys[UBound($deleteKeys) + 1]
		 $deleteKeys[UBound($deleteKeys) - 1] = $i
	  EndIf
   Next

   For $i = UBound($deleteKeys) - 1 to 0 Step -1
	  _ArrayDelete($aActions, $deleteKeys[$i])
   Next

   ; Finally, do a reverse match and kill - remove any entries from the cache if that action doesn't exist anymore
   ; WAIT - THIS IS COMPLICATED. I'LL BUILD IT LATER. https://www.autoitscript.com/autoit3/docs/functions/IniDelete.htm
EndIf

If UBound($aActions) = 0 Then
   If $reportingLevel > 2 Then AddOutputLine("Nothing to do. All defined actions exist in the cache and have been run previously.")
   AllComplete(1)
EndIf

; Count the total number of actions required
$totalActions = 0
For $i = 0 To UBound($aActions) - 1
   $totalActions += UBound($aActions[$i][1])
Next

; Display the output window if required
If $reportingLevel > 0 Then
   $progressWindow = GUICreate("User Settings", 300, 90, 80, 80)
   $guiGroupLabel = GUICtrlCreateLabel("Applying 'General Purpose' Configuration Group", 10, 10, 280)
   $guiActionLabel = GUICtrlCreateLabel("Applying PuTTy Connection Settings...", 10, 30, 280)
   $guiProgress = GUICtrlCreateProgress(10, 55, 280, 20)
   GUICtrlSetData($guiProgress, 50)
   GUISetState()
EndIf

; Loop through each action
$counter = 0
AutoItSetOption ("ExpandEnvStrings", 1)
For $i = 0 to UBound($aActions) - 1
   If $reportingLevel > 0 Then GUICtrlSetData($guiGroupLabel, "Applying '" & $aActions[$i][0] & "' Configuration Group Settings")

   For $n = 0 to UBound($aActions[$i][1]) - 1
	  $counter += 1;

	  If $reportingLevel > 0 Then
		 GUICtrlSetData($guiActionLabel, ($aActions[$i][1])[$n][0] & "...")
		 GUICtrlSetData($guiProgress, ($counter / $totalActions) * 100)
	  EndIf

	  RunWait(($aActions[$i][1])[$n][1], "")

	  ; Write the action cache
	  IniWrite($cacheFile, $aActions[$i][0], ($aActions[$i][1])[$n][0], 1)
   Next
Next

If $reportingLevel > 0 Then GUIDelete($progressWindow)
If $reportingLevel > 2 Then AddOutputLine("Complete. All required actions have been run.")
AllComplete(0)

Func DisplayHelp()
   Local $iMsg, $idExit

   $helpMessage = "Usage:	UserSettings.exe [flags]" & @CRLF & @CRLF
   $helpMessage &= "Flags:" & @CRLF
   $helpMessage &= "  /?		Display help message. Note that all other flags are ignored if this is specified." & @CRLF & @CRLF
   $helpMessage &= "  /v		Verbose - Display error and debug messages (Default: Fail silently, no debug messages)" & @CRLF
   $helpMessage &= "  /e		Errors - Display error messages (Default: Fail silently)" & @CRLF
   $helpMessage &= "  /s		Silent - Don't display progress window (Default: Display progress window if action is required)" & @CRLF
   $helpMessage &= "  Note that the above three flags are contracdictory. If more than one is specified, whichever comes last overrides its predecessors" & @CRLF & @CRLF
   $helpMessage &= "  /f		Force - Ignore history of previously applied items and re-apply all configurations" & @CRLF & @CRLF
   $helpMessage &= "  -o ConfigOU	Specify the active directory organizational unit that contains settings groups (Default: ""Computer Configuration Groups"")" & @CRLF
   $helpMessage &= "  -l Directory	Specify the folder that contains the .ini files for each group (Default: Same directory as executable)" & @CRLF
   $helpMessage &= "  -c Directory	Spefify the folder that contains the cache .ini file (Default: %APPDATA%\UserSettingsTool)"

   GUICreate($msgBoxTitle, 800, 270)

   $idEdit = GUICtrlCreateEdit($helpMessage, 0, 0, 800, 230, $ES_READONLY)
   $idExit = GUICtrlCreateButton("Exit", 740, 240, 50, 20)

   GUISetState()

   Do
	  $iMsg = GUIGetMsg()
   Until $iMsg = $GUI_EVENT_CLOSE Or $iMsg = $idExit

   Exit(0)
EndFunc

Func DisplayVerbose()
   GUICreate($msgBoxTitle, 800, 270)
   $idListView = GUICtrlCreateListView("TimeStamp|Feedback|", 0, 0, 800, 230, $ES_READONLY)
   $idVerboseExit = GUICtrlCreateButton("Exit", 740, 240, 50, 20)
   GUICtrlSetState($idVerboseExit, $GUI_DISABLE)

   GUISetState()
EndFunc

Func AddOutputLine($outputText)
   GUICtrlCreateListViewItem(@HOUR & ":" & @MIN & ":" & @SEC & "|" & $outputText, $idListView)
   _GUICtrlListView_SetColumnWidth ($idListView, 0, $LVSCW_AUTOSIZE)
   _GUICtrlListView_SetColumnWidth ($idListView, 1, $LVSCW_AUTOSIZE)
EndFunc

Func CriticalStop($exitMessage)
   If $reportingLevel = 2 Then MsgBox($MB_OK + $MB_ICONERROR, $msgBoxTitle, $exitMessage)
   If $reportingLevel = 3 Then AddOutputLine("FAILURE: " & $exitMessage)
EndFunc

Func AllComplete($ExitCode = 0)
   If $reportingLevel < 3 Then
	  Exit($ExitCode)
   Else
	  AddOutputLine("Exiting with code " & $ExitCode)
	  GUICtrlSetState($idVerboseExit, $GUI_ENABLE)

	  Do
		 $iMsg = GUIGetMsg()
	  Until $iMsg = $GUI_EVENT_CLOSE Or $iMsg = $idVerboseExit

	  Exit($ExitCode)
   EndIF
EndFunc