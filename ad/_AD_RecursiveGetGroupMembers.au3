#AutoIt3Wrapper_AU3Check_Parameters= -d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#AutoIt3Wrapper_AU3Check_Stop_OnWarning=Y
; *****************************************************************************
; Example 1
; Get a list of all groups the current user is a member of.
; Then get a recursively searched list of all group members for the first group
; Recursion level is 10, inherited group names will be returned.
; *****************************************************************************
#include <AD.au3>

Global $aGroups[1], $aMembers[1]

; Open Connection to the Active Directory
_AD_Open()
If @error Then Exit MsgBox(16, "Active Directory Example Skript", "Function _AD_Open encountered a problem. @error = " & @error & ", @extended = " & @extended)

; Get an array of group names (FQDN) that the user is immediately a member of
$aGroups = _AD_GetUserGroups()
If @error > 0 Then
	MsgBox(64, "Active Directory Functions - Example 1", "The current user is not a member of any group")
Else
	; Get a recursively searched list of members for the first group
$agroups[1] = "be-it"
	$aMembers = _AD_RecursiveGetGroupMembersEX($aGroups[1], 10, True)
	If @error > 0 Then
		MsgBox(64, "Active Directory Functions - Example 1", "The group '" & $aGroups[1] & "' has no members")
	Else
		_ArrayDisplay($aMembers, "Active Directory Functions - Example 1 - Recursive list of members for group '" & $aGroups[1] & "'")
	EndIf
EndIf

; Close Connection to the Active Directory
_AD_Close()

; #FUNCTION# ====================================================================================================================
; Name...........: _AD_RecursiveGetGroupMembers
; Description ...: Takes a group and recursively returns a list of groups and members of the group.
; Syntax.........: _AD_RecursiveGetGroupMembers($sGroup[, $iDepth = 10[, $bListInherited = True[, $bFQDN = True]]])
; Parameters ....: $sGroup         - Group for which the members should to be returned. Can be specified as Fully Qualified Domain Name (FQDN) or sAMAccountName
;                  $iDepth         - Optional: Maximum depth of recursion (default = 10)
;                  $bListInherited - Optional: Defines if the function returns the group it is a member of (default = True)
;                  $bFQDN          - Optional: Specifies the attribute to be returned. True = distinguishedName (FQDN), False = SamAccountName (default = True)
; Return values .: Success - Returns an one-based one dimensional array of group or member names (FQDN or sAMAccountName)
;                  Failure - "", sets @error to:
;                  |1 - Specified group does not exist
; Author ........: Jonathan Clelland
; Modified.......: water
; Remarks .......: This function traverses the groups in the specified group until the maximum depth is reached.
;                  if $bListInherited = True the return is the FQDN or sAMAccountname of the group or member and the FQDN(s) or sAMAccountname(s) of the group it
;                  is a member of, seperated by '|'(s) if flag $bListInherited is set to True.
;+
;                  If flag $bListInherited is set to False then the group/member names are sorted and only unique entries are returned.
; Related .......: _AD_GetGroupMembers
; Link ..........:
; Example .......: Yes
; ===============================================================================================================================
Func _AD_RecursiveGetGroupMembersEX($sGroup, $iDepth = 10, $bListInherited = True, $bFQDN = True)

	If _AD_ObjectExists($sGroup) = 0 Then Return SetError(1, 0, "")
	If StringMid($sGroup, 3, 1) <> "=" Then $sGroup = _AD_SamAccountNameToFQDN($sGroup)
	Local $iCount1, $iCount2
	Local $sField = "distinguishedName"
	If Not $bFQDN Then $sField = "samaccountname"
	$__oAD_Command.CommandText = "<LDAP://" & $sAD_HostServer & "/" & $sAD_DNSDomain & ">;(memberof=" & $sGroup & ");" & $sField & ",ObjectClass;subtree"
	Local $oRecordSet = $__oAD_Command.Execute
	Local $aMembers[$oRecordSet.RecordCount + 1] = [0]
	If $oRecordSet.RecordCount = 0 Then Return $aMembers
	$oRecordSet.MoveFirst
	$iCount1 = 1
	Local $aTempMembers[1]
	Do
		ConsoleWrite("$sGroup: " & $sGroup & @LF)
		Local $aObjectClass = $oRecordSet.Fields(1).Value
		Local $bFound = False
		For $i = 0 To UBound($aObjectClass) - 1
			If $aObjectClass[$i] = "group" Then $bFound = True
		Next
		If Not $bFound Then
			$aMembers[$iCount1] = $oRecordSet.Fields(0).Value
			;		If $oRecordSet.Fields(1).Value = "group" Then ContinueLoop
			If $iDepth > 0 Then
				$aTempMembers = _AD_RecursiveGetGroupMembersEX($aMembers[$iCount1], $iDepth - 1, $bListInherited, $bFQDN)
				If $bListInherited Then
					For $iCount2 = 1 To $aTempMembers[0]
						$aTempMembers[$iCount2] &= "|" & $aMembers[$iCount1]
					Next
				EndIf
				_ArrayDelete($aTempMembers, 0)
				_ArrayConcatenate($aMembers, $aTempMembers)
			EndIf
		EndIf
		$iCount1 += 1
		$oRecordSet.MoveNext
	Until $oRecordSet.EOF
	$oRecordSet.Close
	If $bListInherited = False Then
		_ArraySort($aMembers, 0, 1)
		$aMembers = _ArrayUnique($aMembers, 1, 1)
	EndIf
	$aMembers[0] = UBound($aMembers) - 1
	Return $aMembers

EndFunc   ;==>_AD_RecursiveGetGroupMembers